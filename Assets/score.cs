﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class score : MonoBehaviour {
    public static int scorevalue = 0;
    Text Score;
    public Canvas replay;
	
	void Start () {
        Score = GetComponent<Text>();
       replay.enabled = false;
        scorevalue = 0;
	}
	
	
	void Update () {
        Score.text = "score:" + scorevalue;
        if (scorevalue <= -1)
        {
            replay.enabled = true;
        }
	}
    public void reload()
    {
       
        SceneManager.LoadScene("aj",LoadSceneMode.Single);
    }
   
}
